#Infrastructure du projet

![Infrastructure du projet](_img/infrastructure_boutiques.PNG)

La production et la préproduction sont hébergés sur le même serveur dans deux conteneurs différents. Ils partagent la même
base de donnée.


![Infrastructure du projet](_img/infrastructure_boutiques_2.PNG)

Lors de la création d'une nouvelle boutique il faut :
- que le client pointe l'URL créé sur l'adresse IP de l'hébergeur
- ajouter l'URL dans le virtual-host (dans le docker-compose)
- indiquer l'URL dans la table boutique_aliases de la bdd.



**Attention**

1. Les crons ne tournent pas en préprod.
2. Les mails sont interceptés sur MailTrap (il faut configurer mailtrap dans le ``.env``).

