#Installation du projet

## Pré-requis

- Php : 7.2
- Laravel : 5.6
- Mysql : 8.0.13

##Installer le projet
Faire un clone du projet qui se situe sur un dépôt bitbucket ``git clone https://delta_juliette@bitbucket.org/deltaimport/ppv3.git``.

Copier la base de données qui se situe à l'adresse http://dedie2.test et configurer son environnement ``.env``.
**Attention** : la base de donnée contient une table très lourde qui peut faire planter l'upload. Il s'agit de la table ``communications``.
Si l'upload ne se fait pas, il faut alors ne prendre que la structure de la table ``communications``.


Ne pas oublier de configurer mailtrap pour l'envoie des mails en local.

### Build le css
Le css est build à partir des fichiers scss grâce à une commande personnalisée ``artisan scss:build {codeboutique}``- (mettre "all" pour toutes les boutiques)

##Preproduction
La préprod contient deux URLs :
- ``preprod-eshop.pepitegoodies.com``
- ``test-eshop.pepitegoodies.com/`` pour l'URL que le client et Chloé peuvent utiliser pour les tests.

Afin de sélectionner la boutique à afficher, il faut mettre l'URL à la bonne boutique dans la table ``boutique_aliases``;


**Attention** :
La pré-production n'a pas été configurée pour faire tourner les crons. 

