#Configuration approbation

Pour mettre en place une approbation pour un utilisateur il faut que l'utilisateur ait un approbateur et que le process soit un **process spécifique à l'approbation**.


## Ajouter approbation utilisateur

Pour ajouter un approbateur il y a deux façons de faire :
 - ajouter l'approbateur directement en éditant l'utilisateur
 - insérer les approbateurs directement dans un fichier d'importation (préférable lorsqu'il y a de nombreux utilisateurs concernés)


### Ajout approbateur avec édition de l'utilisateur

Dans le backoffice, dans l'onglet Boutique > Utilisateurs, dans l'édition de l'utilisateur, il faut ajouter un ou des approbateurs.
Plusieurs utilisateurs peuvent être sélectionnés en appuyant sur Ctrl au moment de cliquer sur un nom. 

![Paramétrage consolidation](_img/ajout_approbateur_utilisateur.jpg)

Pour ne plus avoir d'approbateurs, il suffit de sélectionner ``--Aucun approbateur--``


### Ajout approbateur avec fichier d'importation

Dans le backoffice, dans l'onglet Importer un fichier master, sélectionner la boutique désirée et le fichier d'importation qui convient.

![Paramétrage consolidation](_img/ajout_approbateurs_fichier.jpg)

Dans l'onglet ``approvers`` du fichier Excell, il faut mettre le mail de l'utilisateur ainsi que le ou les mails (séparés par une virgule) des approbateurs. 

Renseigner un utilisateur et laisser le champ des approbateurs vide aura pour effet de remettre les approbateurs de l'utilisateur en question à  ``--Aucun approbateur--``.