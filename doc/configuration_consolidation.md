#Configuration consolidation

_Attention :_ Les scripts pour la consolidation passent tous les matins à minuit.
On ne peut pas paramétrer une consolidation pour le jour-même car elle ne sera pas
prise en compte. 


##Dans les configurations générales de la boutique

![Paramétrage consolidation](_img/parametrage_consolidation.jpg)

Le ``jour début de consolidation auto`` permet d'indiquer quel jour de la semaine le script doit créer une consolidation pour
les produits de la boutique ayant des tarifs consolidés. 
La ``durée de consolidation`` indique combien de jours après le jour où sera lancé la consolidation
elle doit se terminer. 

*Par exemple : Nous sommes le mercredi et on veut une consolidation du lundi qui vient et pour une durée de
7 jours. On mettra alors ``lundi`` et ``7`` jours pour la durée.*

**Rappel** : on ne peut pas lancer une consolidation pour le jour-même car le script de la journée créant la 
consolidation s'exécute à minuit. 


##Impératifs pour créer une consolidation

- Paramétrer la consolidation au minimum le jour avant le début (paramètre, nombre de jour, produits concernés)
- Avoir des prix consolidés pour les produits concernés

Si la consolidation n'est pas paramétrée avant le jour de début, elle sera retardée d'une semaine (dans notre
exemple plus haut : le lundi d'après).