# Créer un process de commande

Le process se crée dans le back-office dans ``Données partagées `` > ``Process de commande``. 

Après le récapitulatif de commande, le process s'exécutera et suivra les éléments qu'il contient dans l'ordre et en
fonction des conditions rencontrées. 

## Ajout d'une condition
Les conditions sont des étapes spécifiques qui peuvent avoir deux choix en fonction de si la condition est remplie. 
Il n'est pas obligatoire d'ajouter une action si la condition n'est pas remplie : auquel cas le process restera à
l'étape précédente jusqu'à ce que la condition soit remplie.

En terme de code, les conditions sont créées dans le ``StepManagerService``.


## Ajout d'un élément

Les différentes étapes sont créées dans ``Services`` > ``Steps``.

## Lancement du process

Les process non terminés sont lancés grâce à un cron qui lance toutes les heures à la 3eme minute ``*/3 * * * *`` 
la commande ``php artisan orders:process``. Il est possible de lancer le process d'une commande particulière en 
ajoutant l'id de la commande à la commande (orders:process {id de la commande}).