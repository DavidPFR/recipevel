#Consolidation
___

Lors d'une consolidation le process n'est pas lancée avant que la période soit terminée
et que tous les ``produit_consolidations`` aient une date inférieure à la date actuelle.

La consolidation est aussi appelée "commande groupée".

##Lancer une consolidation

Une période de consolidation pour un produit ne peut se lancer que si aucune autre période
n'est active pour ce même produit en même temps. 

###Paramètrages
####Période de consolidation globale à tous les produits de la boutique

Une période de consolidation globale se configure dans le backoffice sous l'onglet **_Boutique -> Configuration générale_** de la sorte :
- se lance au jour de la semaine indiqué par le champ **Jour début de consolidation auto.** 
- pour une durée de xx jours indiqués dans le champ **Durée consolidation auto.**. 

*Par exemple :*

*Pour un paramètre comme le suivant, une période de consolidation de 7 jours débutera pour les produits 
tous les lundi à la condition qu'aucune consolidation ne soit déjà en cours.
![Jour conso: lundi, durée conso : 7](consolidation_autoconso_parameters.jpg)*


####Période de consolidation spécifique à un produit

Une période de consolidation s.pécifique à un produit configure dans le backoffice sous l'onglet **_Boutique -> Produit _**. Il faut éditer le 
produit concerné et aller dans l'onglet **Consolidations**.


## Problèmes déjà rencontrés et solutions apportées
### Problème de paramétrage de la consolidation
Un des problèmes déjà rencontré était la cause d'un mauvais paramétrage en backoffice. Les commandes passées précédement n'étaient pas prise
en compte dans la consolidation empêchant les bons prix de s'afficher. 

Pour résoudre ce problème il a fallut:
- dans la table ``produit_consolidations``:
  - date ``start`` et ``end`` : mettre les dates voulues
  - ``active`` : repasser à 1 
  - ``quantity`` : quantité initiale avec laquelle on veut commencer la consolidation (généralement 0)
- dans la table ``commande_produits``, pour toutes les commandes concernées modifier les champs ``updated_at`` à **NOW()**, ``consodate`` à **NULL** 
pour les commandes ayant le produit_id / la référence voulu. 
  - *ex : ``UPDATE commande_produits SET updated_at = NOW(), consodate = NULL WHERE `produit_id` = 194 AND `reference` = 'SE20220';``*
- dans la table ``commandes`` modifier le champ ``termine`` à 0 ainsi que les champs ``process_step_id`` et ``commande_statut_id`` aux valeurs souhaitées
  - *ex: ``UPDATE commandes c JOIN commande_produits cp ON c.id = cp.commande_id SET termine = 0, process_step_id = 98, commande_statut_id = 2 WHERE cp.produit_id = 194 AND cp.reference = 'SE20220';``*

La consolidation sera lancée automatiquement au prochain jour à minuit mais elle peut également être appelée manuellement avec la commande ``php artisan conso:calculate``.