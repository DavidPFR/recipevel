#Fonctionalités

## Approbation

Consiste à ce qu'une commande doit être approuvée par un utilisateur définit comme approbateur. 

Ne fonctionne que si l'utilisateur a un approbateur (se configure dans le backoffice pour chaque utilisateur) et si le process (pour le moment seul un process
pour les commandes stock a été créé) est sélectionné. 


*Doc lié*

- [Configuration approbation](configuration_approbation.md)


## Consolidation

Certaines commandes peuvent être passées en consolidation. Le prix dépendra du nombre de produits achetés
pendant la période de consolidation.

Des crons tournent tous les jours quelques minutes après minuit pour :
- consolider les périodes de consolidation qui sont arrivées à terme
- lancer des consolidations 


### Consolider les périodes de consolidation

Lorsqu'une période de consolidation arrive à terme, la quantité de produits des commandes concernés sont calculés 
ce qui permet de déterminer le prix des produits 


*Doc lié*

- [Configuration consolidation](configuration_consolidation.md)
- [Problèmes connus](known_problems/issues_consolidation.md)


## Process de commande

Dans les paramètres généraux de la boutique, il y a deux process à sélectionner : 
- le process pour les commandes stock
- le process pour les commandes consolidées


*Doc lié*

- [Création d'un process](create_order_process.md)


## Configuration boutique

Les boutiques sont paramétrées en différents endroits dans le backoffice ``/back``. 

Certains paramètres sont propres à toutes les boutiques et se trouvent dans l'onglet ``Données partagées``.
D'autres sont spécifiques à la boutique et se trouvent dans l'onglet ``Boutique``.