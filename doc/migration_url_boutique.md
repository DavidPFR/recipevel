#Migration des boutiques vers l'URL du client

Les boutiques sont créés sur des URLS temporaires avant d'être migrer sur l'URL décidée par le client.
Lors de cette migration, plusieures actions sont à réaliser.

##Ajouter l'adresse URL au docker-compose
Sur le serveur, il faut éditer le fichier docker-compose.yml ``sudo nano docker-compose.yml`` et y ajouter la nouvelle URL à la fois dans ``VIRTUAL_HOST`` et dans ``LETSESNCRYPT_HOST``.
Ne pas enlever l'ancienne (il faudrait si on le faisait, remodifier le chemin de toutes les images).

Une fois que c'est fait, il faut **redémarrer docker-compose** (docker_compose down && docker-compoe up -d).


##Modifier l'URL dans la base de donnée
Sur phpmyadmin, dans la table ``boutique aliases`` il faut modifier l'ancienne URL par la nouvelle URL.

Une fois ces deux étapes réalisées, la boutique est disponible sur la nouvelle URL du client. 