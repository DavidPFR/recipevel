# Mise en production

Pour mettre en production il faut aller sur le serveur (une solution comme Solar Putty permet de se connecter au serveur).

## Serveur de production
Pour accéder à la production
``cd /srv/boutiques_promoplus/public_html``

Pour mettre en prod ``sudo git pull`` (le serveur demandera le mot de passe)

En fonction des éléments mis à jour il faudra penser à : 
- faire les migrations ``docker-compose exec boutiquesv2-php php artisan migrate``
- lancer la commande pour build les assets ``docker-compose exec boutiquesV2-php php artisan scss:build all`` all peut être 
remplacé par le code de la boutique dans le cas où on ne voudrait reconstruire les assets que pour une boutique.


## Serveur de préproduction
Pour accéder à la pré-production
``cd /srv/boutiques_promoplus_preprod/public_html``

Pour mettre en pré-prod ``sudo git pull`` (le serveur demandera le mot de passe)

En fonction des éléments mis à jour il faudra penser à :
- faire les migrations ``docker-compose exec preprod-eshop-php php artisan migrate``
- lancer la commande pour build les assets ``docker-compose exec preprod-eshop-php php artisan scss:build all`` all peut être
  remplacé par le code de la boutique dans le cas où on ne voudrait reconstruire les assets que pour une boutique.
