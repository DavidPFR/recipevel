#Punchout

Certaines boutiques disposent d'une connection via punchout.
C'est le cas de Suez par exemple. Grdf a également un chantier punchout en cours. 

Les deux punchout ne fonctionnent pas de la même façon. Cela provient probablement de l'outil utilisa par le client 
qui n'est pas le même. Les XML renvoyés ne sont pas identiques. 

## Fonctionnement 

Il y a un override du BoutiqueController pour chaque boutique concernée qui ajoute 
la méthode "punchLogin" et "afterPunchLogin" et qui override la méthode "recaputilatif".

### Fonction punchLogin()

Cette fonction permet de récupérer les identifiants de connection de l'utilisateur. Si un compte correspond à 
l'adresse email envoyé, on récupère le compte associé. Si ce n'est pas le cas, on crée un compte. On stock également
les informations en session et on renvoie un XML de réponse au logiciel du client.

### Fonction afterPunchLogin()

Cette fonction permet de connecter l'utilisateur et de le renvoyer sur la boutique.  

### Fonction recaputilatif()

La fonction recapitulatif est overridée. Elle permet de préparer le XML contenant le contenu du panier et différentes 
informations utiles à renvoyer à l'outil du client.




